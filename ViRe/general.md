#	Valós Idejű Rendszerek

##	Tanár

-	Előadás: Brassai Sándor Tihamér
-	Labor:	Hajdú Szabolcs

##	Vizsgajegy

2 Opció van

###	I.

-	Vizsga (50%)
-	Labor (50%)

###	II.

-	Projekt

A projekthez a Szoftvert és melléje a Teszteket is meg kell csinálni.
Vagy saját Teszter, vagy TraceAlyzer (Nem tudjuk mi a neve, de van valami)

## ESZKÖZÖK

### Hardware

Zybo Zynq 7000 chip-et fogjuk használni
2 X ARM CORE

Egyéb hardware-t vagy kapunk, vagy megépítünk

###	Operációs Rendszer

-	FreeRTOS
-	Linux

####	FreeRTOS

A FreeRTOS egy valós idejű OS. Taszkok, üzenetsorok, semaforok, osztott memória, tesztek.


