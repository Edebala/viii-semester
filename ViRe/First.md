#	Valós idejű Rendszerek I. Előadás

## Operációs Rendszer:

	-	Feladatok ütemezése
	- Állományok lemezre való írása
	- Adatok hálózaton való átküldése

## Valós Idejű operációs rendszer

Párhuzamos környezet, amely biztosítja, hogy egy eseményre a válasz megbecsülhető határidőn belül
	
"those systems in which the correctness of the system depends not only on the logical
result of the computation, but also on the time at which the results are produced"
-J.Stankovic "Misconceptions About Real-Time Computing", IEEE Computer, 21(10), October 1988

### Some definitions

-	Periodic Tasks (Time-driven and recurring at regular intervals)

- Aperiodic Tasks (event-driven)

-	Timing constraint

-	Release Time

-	Deadline

-	Response Time

####	Soft, Firm and Hard Deadlines

-	The instant at which a result is needed is called a deadline.

	-	If the result has utility even after the deadline has passed, 
the deadline is classified as soft, otherwise it is firm.
	-	If a catastrophe could result if a firm deadline is missed, the deadline is hard

###	Real Time Operating System ?

-	A type of an operating system
-	It's all about scheduler
	-	multi user operating system (UNIX) - fair amount of processing time
	- desktop operating system (Windows) - remain responsive to its user
	- ...

RTOS scheduler focuses on predictable execution pattern

##	Free RTOS

-	A Real Time Operating System

-	Written by Richard Barry & FreeRTOS Team

-	Owned by Real Time Engineers Ltd but free to use

-	Huge number of users all over the world

	-	6000 Download per month

- Simple but very powerful

###	Why to use real-time kernel

-	Modularity
- Teeeam develoment
- Easier testing
- Code reuse
- Improved efficiency
- Idle time
- Power management
- Flexible Interrupt Handling
- Mixed processing requirement

##	Value Proposition

-	The unprecedented global success of FreeRTOS comes from its
compelling value proposition; FreeRTOS is professionally developed,
strictly quality cotrolled robust


Mastering of FreeRTOS
