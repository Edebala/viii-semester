Egy ütemező algoritmus lehet
-	preemptive és non-preemptive
-	Statikus és dinamikus

Preemptive - megszakítható
Non-Preemptive - nem megszakítható

##	Two common scheduling schemes

###	Rate monotonic
-	Static priority scheme
-	Preemption Required
-	Simple to implement
-	Nice properties

###	Earliest deadline first
-	Dynamic priority scheme
-	Preemption Required
-	Harder to implement
-	Very nice properties

##	Data types

###	TickType_t

###	Sokféle Heap

####	Heap_1

####	Heap_2

-	Ugyanakkora blokkokra osztja a memóriát
 
####	Heap_3

####	Heap_4

-	Próbál minél nagyobb szabad memóriaterületeket összekötni
 
####	Heap_5

###	Ütemező algoritmusok

-	Blocked - Ready - Running - Block
