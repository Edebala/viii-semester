#include <stdio.h>
#include <string.h>

char CaesarPermutation(char c, int n){
	if(c >= 'a' && c <= 'z')
		return 'a' + (('z'-'a' + 1 + c - 'a' + n) % ('z'-'a' + 1));
	if(c >= 'A' && c <= 'Z')
		return 'A' + (('z'-'a' + 1 + c - 'A' + n) % ('Z'-'A' + 1));
	return c;
}

char CaesarPermutationUpperCase(char c, int n){
	if(c >= 'A' && c <= 'Z')
		return 'A' + (('z'-'a' + 1 + c - 'A' + n) % ('Z'-'A' + 1));
	return c;
}

void Caesar(char* s, int n){
	int l = strlen(s);
	for(int i=0;i<l;i++){
		s[i] = CaesarPermutation(s[i],n);
	}
}

void CaesarEncryptTextFile(char* fromFile,char* toFile, int n){
	FILE* fR = fopen(fromFile,"r");
	FILE* fW = fopen(toFile,"w");
	char c;
	while(fscanf(fR,"%c",&c) != EOF){
		c = CaesarPermutation(c,n);
		fprintf(fW,"%c",c);
	}
		fclose(fR);
		fclose(fW);
}

void CaesarEncryptTextFileUpperCase(char* fromFile,char* toFile, int n){
	FILE* fR = fopen(fromFile,"r");
	FILE* fW = fopen(toFile,"w");
	char c;
	while(fscanf(fR,"%c",&c) != EOF){
		if(c >= 'a' && c <= 'z')
			c += 'A' - 'a';
		c = CaesarPermutationUpperCase(c,n);
		fprintf(fW,"%c",c);
	}
		fclose(fR);
		fclose(fW);
}

int main(){
	char s[70] = "Hellobello! Ez egy szoveg. A szovegek MENO dolgok. ABCD abcd\0";
	printf("%s\n",s);
	Caesar(s,-3);
	printf("%s\n",s);
	Caesar(s,3);
	printf("%s\n",s);

	for(int i=0;i<26;i++){
		char fName[12] = "outputA.txt";
		fName[6] = 'A' + i;
		CaesarEncryptTextFileUpperCase("input.txt",fName,i);
	}
}
