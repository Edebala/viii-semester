[Back](index) 

#	Rendszerelmélet Ismétlés

-	Diszkrét átviteli függvény
-	Pólus-Zérus ábra
-	Stabilitás, Kauzalitás
-	Bode Diagram


## Mintavételes LTI Rendszer

x(t), t c Z - Matekes leírás

x[n], n eleme Z diszkrét - differencia egyenletek
x(t), t eleme R folytonos - differenciál egyenletek

h[n]
h(n)

y[n] = h[n] * x[n], ahol * a konvolúció

Periodicitás

Létezik T eleme R
x(t) = x(t+T)
Bármely t eleme R

Determinisztikus jelek

Folytonos és diszkrét idejű jelek

Átlépés folytonos időtartományból diszkrét időtartományba

##	Periodikus vagy aperiodikus jelek

Determinisztikus vagy Véletlenszerű jelek

Fontos jeleink:
-	Dirak
-	Egységugrás
-	Szinusz
	x(t) = A*sin( ωt + T)
