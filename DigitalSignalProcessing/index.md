[A Tantárgyról](Tantargy)

## Tartalom

[1. Rendszerelmélet Ismétés](RendszIsmetles)

[2. Diszkretizálás, adat hibák](Diszkretizalas)

[3. Jelek rekonstruálása](JelekRekonstrualasa)

[4. Fourier analízis](FourierAnalizis)

[5. Szűrő struktúrák](SzuroStrukturak)

[6. Véges Impulzusválaszú Szűrők](FIR)

[7. Végtelen Impulzusválaszú Szűrők](IIR)

[8. Modern eljárások - STFT](STFT)

###	Gyakorlat tematika

C, Python

[1.	Bevezető az STM32CUBEIDE környezetbe - C](FirstLab)
2.	Mintavételezés és jelek rekonstruálása (ADC, DAC) - C
3.	Hardveres gyorsítás - egész, fixpontos, lebegőpontos számok - C
4.	Konvolúció programmozása - C
5.	Spektrum. zaj típusok - Python
6.	Fourier transzformált - Python
7.	Fourier transzformált - C
8.	Mozgó átlag szűrő - C
9.	FIR szűrők tervezése - Python
10. FIR szűrők alkalmazása - C
11.	Exponenciális átlag szűrő - C
12.	IIR szűrők tervezése - Python
13.	IIR Szűrők alkalmazása - C

ADC - FPGA - DAC
FPGA - ARM

## Tantárgyi elvárások

### Labor

Tervezzünk Szűrőt, és bizonyítsuk be, hogy jól működik


